package nl.egulden.discordbot.services.discord

import com.google.inject.AbstractModule
import net.dv8tion.jda.api.entities._
import net.dv8tion.jda.api.hooks.IEventManager
import net.dv8tion.jda.api.managers.{AudioManager, DirectAudioController, Presence}
import net.dv8tion.jda.api.requests.restaction.GuildAction
import net.dv8tion.jda.api.requests.{GatewayIntent, RestAction}
import net.dv8tion.jda.api.sharding.ShardManager
import net.dv8tion.jda.api.utils.cache.{CacheView, SnowflakeCacheView}
import net.dv8tion.jda.api.{AccountType, JDA, JDABuilder, Permission}
import okhttp3.OkHttpClient
import play.api.{Configuration, Environment, Mode}

import java.util
import java.util.concurrent.{ExecutorService, ScheduledExecutorService}

class DiscordModule(environment: Environment,
                    configuration: Configuration)
  extends AbstractModule {

  override def configure(): Unit = {
    environment.mode match {
      case mode if (mode == Mode.Dev || mode == Mode.Prod) =>
        bind(classOf[DiscordMessageListener]).asEagerSingleton()
        bind(classOf[TransactionEventListener]).asEagerSingleton()
        bind(classOf[JDA]).toInstance(discordClient())

      case _ =>
        bind(classOf[JDA]).toInstance(testingDiscordClient())
    }
  }

  private def discordClient(): JDA = {
    val token = configuration.get[String]("discord.token")

    JDABuilder.create(
      GatewayIntent.GUILD_MESSAGES,
      GatewayIntent.DIRECT_MESSAGES,
      GatewayIntent.GUILD_INVITES,
    )
      .setToken(token)
      .build()
  }

  private def testingDiscordClient(): JDA = {
    new JDA {
      override def addEventListener(listeners: Any*): Unit = ???
      override def awaitStatus(status: JDA.Status): JDA = ???
      override def awaitStatus(status: JDA.Status, status2: JDA.Status*): JDA = ???
      override def cancelRequests(): Int = ???
      override def createGuild(name: String): GuildAction = ???
      override def createGuildFromTemplate(x$1: String, x$2: String, x$3: net.dv8tion.jda.api.entities.Icon): net.dv8tion.jda.api.requests.RestAction[Void] = ???
      override def deleteCommandById(x$1: String): net.dv8tion.jda.api.requests.RestAction[Void] = ???
      override def editCommandById(x$1: String): net.dv8tion.jda.api.requests.restaction.CommandEditAction = ???
      override def getAccountType: AccountType = ???
      override def getAudioManagerCache: CacheView[AudioManager] = ???
      override def getCacheFlags(): java.util.EnumSet[net.dv8tion.jda.api.utils.cache.CacheFlag] = ???
      override def getCallbackPool: ExecutorService = ???
      override def getCategoryCache: SnowflakeCacheView[Category] = ???
      override def getDirectAudioController: DirectAudioController = ???
      override def getEmoteCache: SnowflakeCacheView[Emote] = ???
      override def getEventManager: IEventManager = ???
      override def getGatewayIntents(): util.EnumSet[GatewayIntent] = ???
      override def getGatewayPing: Long = ???
      override def getGatewayPool: ScheduledExecutorService = ???
      override def getGuildCache: SnowflakeCacheView[Guild] = ???
      override def getHttpClient: OkHttpClient = ???
      override def getInviteUrl(permissions: Permission*): String = ???
      override def getInviteUrl(permissions: util.Collection[Permission]): String = ???
      override def getMaxReconnectDelay: Int = ???
      override def getMutualGuilds(users: User*): util.List[Guild] = ???
      override def getMutualGuilds(users: util.Collection[User]): util.List[Guild] = ???
      override def getPresence: Presence = ???
      override def getPrivateChannelCache: SnowflakeCacheView[PrivateChannel] = ???
      override def getRateLimitPool: ScheduledExecutorService = ???
      override def getRegisteredListeners: util.List[AnyRef] = ???
      override def getResponseTotal: Long = ???
      override def getRoleCache: SnowflakeCacheView[Role] = ???
      override def getSelfUser: SelfUser = ???
      override def getShardInfo: JDA.ShardInfo = ???
      override def getShardManager: ShardManager = ???
      override def getStatus: JDA.Status = ???
      override def getStoreChannelCache: SnowflakeCacheView[StoreChannel] = ???
      override def getTextChannelCache: SnowflakeCacheView[TextChannel] = ???
      override def getToken: String = ???
      override def getUnavailableGuilds(): java.util.Set[String] = ???
      override def getUserCache: SnowflakeCacheView[User] = ???
      override def getVoiceChannelCache: SnowflakeCacheView[VoiceChannel] = ???
      override def isAutoReconnect: Boolean = ???
      override def isBulkDeleteSplittingEnabled: Boolean = ???
      override def isUnavailable(x$1: Long): Boolean = ???
      override def openPrivateChannelById(id: Long): RestAction[PrivateChannel] = ???
      override def removeEventListener(listeners: Any*): Unit = ???
      override def retrieveApplicationInfo(): RestAction[ApplicationInfo] = ???
      override def retrieveCommandById(x$1: String): net.dv8tion.jda.api.requests.RestAction[net.dv8tion.jda.api.interactions.commands.Command] = ???
      override def retrieveCommands(): net.dv8tion.jda.api.requests.RestAction[java.util.List[net.dv8tion.jda.api.interactions.commands.Command]] = ???
      override def retrieveUserById(id: Long): RestAction[User] = ???
      override def retrieveUserById(id: String): RestAction[User] = ???
      override def retrieveUserById(x$1: Long, x$2: Boolean): net.dv8tion.jda.api.requests.RestAction[net.dv8tion.jda.api.entities.User] = ???
      override def retrieveWebhookById(webhookId: String): RestAction[Webhook] = ???
      override def setAutoReconnect(reconnect: Boolean): Unit = ???
      override def setEventManager(manager: IEventManager): Unit = ???
      override def setRequestTimeoutRetry(retryOnTimeout: Boolean): Unit = ???
      override def setRequiredScopes(x$1: java.util.Collection[String]): net.dv8tion.jda.api.JDA = ???
      override def shutdown(): Unit = ???
      override def shutdownNow(): Unit = ???
      override def unloadUser(x$1: Long): Boolean = ???
      override def updateCommands(): net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction = ???
      override def upsertCommand(x$1: net.dv8tion.jda.api.interactions.commands.build.CommandData): net.dv8tion.jda.api.requests.restaction.CommandCreateAction = ???
    }
  }
}

